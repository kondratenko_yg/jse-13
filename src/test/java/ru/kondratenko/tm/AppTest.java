package ru.kondratenko.tm;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        final Application application = new Application();
        System.out.println(application.getProjectController().listProject());
        System.out.println(application.getTaskController().listTask());
        System.out.println();
        application.run("version");
        application.run("version");
        application.run("version");
        application.run("about");
        application.run("version");
        application.run("version");
        application.run("version");
        application.run("version");
        application.run("version");
        application.run("version");
        System.out.println();
        application.run("history");

        assertTrue(true);
    }

}
