package ru.kondratenko.tm.controller;

import java.util.Scanner;

public abstract class AbstractController {

    protected final Scanner scanner = new Scanner(System.in);

    public int inputIndexCheckFormat() {
        final int index;
        try {
            index = Integer.parseInt(scanner.nextLine()) - 1;
        } catch (Throwable t) {
            System.out.println("[WRONG FORMAT]");
            return -1;
        }
        return index;
    }

    public long inputIdCheckFormat() {
        final long id;
        try {
            id = Long.parseLong(scanner.nextLine());
        } catch (Throwable t) {
            System.out.println("[WRONG FORMAT]");
            return 0L;
        }
        return id;
    }

}
